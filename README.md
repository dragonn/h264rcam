# h264rcam

h264rcam provides an easy way to get a Raspberry Pi Camera stream inside web browser.

Unlike mjpeg streamer it can provide a smooth ~20FPS or even more stream without saturate the whole Wi-Fi bandwitch and using a lot of CPU.

This project was inspired by [pi-h264-to-browser](https://github.com/dans98/pi-h264-to-browser) but it was rewriten into Rust to use less system resources (only 8% CPU usage on a Pi Zero W compared too 20% on pi-h264-to-browser)

## Compiling

Since compiling Rust programs can be quite resource intesive the prefered way is to cross-compile.
A binary can be downloaded from the dist folder too.

Prerequirements:
- Any Linux OS on a x86 PC
- Docker
- [Cross](https://github.com/rust-embedded/cross)

After satisfying the prerequirements run `build.sh`.
When the compile finishes inside `target/arm-unknown-linux-gnueabihf/release/` will be placed the executable `h264rcam` with needs to be copied over to Pi. This binary schould work on Raspbian 10 (not tested yet on 11)

## Instaling

1. Create a directory /home/pi/h264rcam (`mkdir /home/pi/h264rcam`)
2. Copy over the binary inside this directory and add execution permisions (`chmod +x /home/pi/h264rcam/h264rcam`)
3. Copy over h264rcam.service into `/etc/systemd/system/h264rcam.service`
4. Enable the service `sudo systemctl enable --now h264rcam`

Coping files can be done with any way with you know (sshfs, scp etc.)

## Usage

By default the stream can be pen on port 8000 of your Pi local IP adress/local DNS. Just open it inside a web browser: `http:/XXX.XXX.XXX.XXX:8000` (replace XXX with you Pi ip adress)

## Configure

Default settings are port 8000, 20 FPS, 1024x576 resolution.
To change those settings create a file `h264rcam.toml` inside /home/pi/h264rcam. A example config file is provided in this repo.

## Know issues

- any new client after the first connection takes a 1-2s to pick up the stream and has a slight bigger delay.

## Demo

Click to play the demo

[![Demo](https://gitlab.com/dragonn/h264rcam/-/raw/master/demo.png?inline=false)](https://gitlab.com/dragonn/h264rcam/-/blob/master/demo.mp4)

## Credits

- [pi-h264-to-browser](https://github.com/dans98/pi-h264-to-browser)
- [rascam](https://raw.githubusercontent.com/pedrosland/rascam/) (currently cloned inside this project to due strange issues of Cargo not resolving git repos in dependencies (I must be doing something wrong))

