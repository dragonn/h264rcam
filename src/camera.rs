use rascam::*;
use std::collections::VecDeque;
use std::fs::File;
use std::io::{BufWriter, Write};
use std::{thread, time};

use std::sync::{
    atomic::{AtomicBool, Ordering},
    Arc, Mutex,
};

use std::time::Instant;

use actix::Addr;

use crate::ws::{CameraFrame, CameraWs};

use tokio::sync::mpsc::{channel, Receiver, Sender};

#[derive(Debug, Clone)]
pub struct Camera {
    config: Arc<crate::config::Config>,
    running: Arc<AtomicBool>,
    camera_info: Arc<Info>,
    sender: Sender<Addr<CameraWs>>,
    receiver: Arc<Mutex<Option<Receiver<Addr<CameraWs>>>>>,
}

impl Camera {
    pub fn new(config: crate::config::Config) -> Self {
        let info = info().unwrap();
        info!("cameras: {:?}", info);
        let (sender, receiver) = channel(8);

        Self {
            config: Arc::new(config),
            running: Default::default(),
            camera_info: Arc::new(info),
            sender,
            receiver: Arc::new(Mutex::new(Some(receiver))),
        }
    }

    pub async fn push(&self, client: Addr<CameraWs>) {
        if !self.running.load(Ordering::Relaxed) {
            self.clone().spawn();
        }
        self.sender.try_send(client);
    }

    fn camera(&self) -> SimpleCamera {
        info!("getting camera!");
        let mut camera = SimpleCamera::new(self.camera_info.cameras[0].clone()).unwrap();
        camera.configure(CameraSettings {
            // shared
            encoding: MMAL_ENCODING_H264,
            width: self.config.width as u32,
            height: self.config.height as u32,
            use_encoder: true,
            // video
            framerate: self.config.fps as u32,
            video_profile: MMAL_VIDEO_PROFILE_H264_HIGH,
            video_level: MMAL_VIDEO_LEVEL_H264_4,
            ..Default::default()
        });
        camera.activate().unwrap();
        info!("canera active end");
        camera
    }

    fn spawn(self) {
        self.running.store(true, Ordering::Relaxed);
        thread::spawn(move || {
            info!("thread started!!");
            let mut camera = self.camera();
            let mut reciver = self.receiver.lock().unwrap().take().unwrap();

            let mut clients = Vec::with_capacity(1);

            if let Ok(client) = reciver.try_recv() {
                clients.push(client);
            }

            let mut start_frames = Vec::with_capacity(2);
            let mut last_frames =
                VecDeque::with_capacity(self.config.buffer as usize * self.config.fps as usize);

            let frame_iter = camera.take_video_frame_writer().unwrap();
            let mut last_camera_restart_pending = false;
            let mut exit = false;

            for frame in frame_iter {
                if start_frames.len() < 2 {
                    start_frames.push(frame.clone());
                }
                if clients.len() >= 1 {
                    last_frames.clear();
                    for client in clients[1..].iter() {
                        client.do_send(CameraFrame(frame.clone()))
                    }
                    clients[0].do_send(CameraFrame(frame));
                } else {
                    if self.config.buffer > 0 {
                        last_frames.push_back(frame);
                        if last_frames.len()
                            > self.config.buffer as usize * self.config.fps as usize
                        {
                            last_frames.pop_front();
                        }
                    }
                }

                if let Ok(client) = reciver.try_recv() {
                    for frame in &start_frames {
                        client.do_send(CameraFrame(frame.clone()));
                    }

                    for frame in &last_frames {
                        client.do_send(CameraFrame(frame.clone()));
                    }
                    clients.push(client);
                }

                clients.retain(|c| c.connected());
            }
            camera.stop();

            info!("stream thread shutdown");
            *self.receiver.lock().unwrap() = Some(reciver);
            self.running.store(false, Ordering::Relaxed);
        });
    }
}
