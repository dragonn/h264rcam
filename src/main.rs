#[macro_use]
extern crate log;

use actix_web::{
    get, middleware::Logger, middleware::NormalizePath, middleware::TrailingSlash, web, App,
    HttpResponse, HttpServer, Responder,
};

mod camera;
mod config;
mod ws;

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    #[cfg(not(debug_assertions))]
    std::env::set_var("RUST_LOG", "info");
    #[cfg(debug_assertions)]
    std::env::set_var("RUST_LOG", "debug");
    #[cfg(debug_assertions)]
    std::env::set_var("RUST_BACKTRACE", "1");

    pretty_env_logger::init_timed();

    let config = config::Config::new();
    let camera = camera::Camera::new(config.clone());

    info!("h264rcam starting: {:?}", config);

    HttpServer::new(move || {
        App::new()
            .data(config.clone())
            .data(camera.clone())
            .wrap(Logger::new("\"%r\" %s %b %T"))
            .wrap(NormalizePath::new(TrailingSlash::Trim))
            .service(get_index)
            .service(get_jmuxer)
            .service(ws::get_camera)
    })
    .backlog(64)
    .bind("0.0.0.0:8000")?
    .run()
    .await
}

#[get("/")]
async fn get_index(config: web::Data<config::Config>) -> impl Responder {
    let html = maud::html! {
        (maud::DOCTYPE)
        meta charset="utf-8";
        meta http-equiv="X-UA-Compatible" content="IE=edge";
        meta name="viewport" content="width=device-width, initial-scale=1.0";
        meta name="description" content="H264RCAM";
        meta name="author" content="Mateusz Schyboll";
        link rel="shortcut icon" href="public/favicon.png";
        title { "h264rcam" }
        style {"    body{
                        margin:0;
                        padding:0;
                        background-color:#303030;
                    }
                    
                    #streamStage{
                        position:fixed;
                        top:0;
                        left:0;
                        width:100%;
                        height:100%;
                    }
                    
                    #streamStage:before {
                        content: '';
                        box-sizing: border-box;
                        position: absolute;
                        top: 50%;
                        left: 50%;
                        width: 2rem;
                        height: 2rem;
                        margin-top: -1rem;
                        margin-left: -1rem;
                    }
                    
                    #stream{
                        max-height: 100%;
                        max-width: 100%;
                        margin: auto;
                        position: absolute;
                        top: 0; left: 0; bottom: 0; right: 0;
                    }"}
        script src=("./js/jmuxer.min.js") {};
        body {
            div id="streamtage" {
                video controls="" autoplay="" muted="" id="stream";
            }
        }
        script {
            (format!(r##"window.onload = function(){{
                var jmuxer = new JMuxer({{
                    node: 'stream',
                    mode: 'video',
                    flushingTime: 0,
                    fps: {},
                    debug: false
                }});

                function ws_init() {{
                    var protocol = 'ws://';
                    if (window.location.protocol === 'https:') {{
                        protocol = 'wss://';
                    }}

                    var ws = new WebSocket(protocol+window.location.host+window.location.pathname+'camera/');
                    ws.binaryType = 'arraybuffer';
                    ws.addEventListener('message',function(event){{
                        if (!document.hidden){{
                            jmuxer.feed({{
                                video: new Uint8Array(event.data)
                            }});				
                        }}
                    }});

                    ws.addEventListener('close',function(event){{
                        setTimeout(ws_init, 250);
                    }});
                }}

                ws_init();
            }}"##, config.fps))     
        }
    };

    HttpResponse::Ok().body(html.into_string())
}

const JMUXER_MIN_JS: &'static str = include_str!("./jmuxer.min.js");

#[get("/js/jmuxer.min.js")]
async fn get_jmuxer(config: web::Data<config::Config>) -> impl Responder {
    HttpResponse::Ok().body(JMUXER_MIN_JS)
}
