use actix::prelude::*;
use actix::{Actor, StreamHandler};
use actix_web::{get, web, HttpRequest, HttpResponse, Responder};
use actix_web_actors::ws;

use std::time::Duration;
use tokio::sync::broadcast::Receiver;

use crate::camera;

#[get("/camera")]
async fn get_camera(
    req: HttpRequest,
    stream: web::Payload,
    camera: web::Data<crate::camera::Camera>,
) -> impl Responder {
    if let Ok((addr, resp)) = ws::start_with_addr(CameraWs, &req, stream) {
        camera.push(addr).await;
        resp
    } else {
        HttpResponse::BadRequest().body("failed creating ws client")
    }
}

// Define HTTP actor
pub struct CameraWs;

impl Actor for CameraWs {
    type Context = ws::WebsocketContext<Self>;
}

/// Handler for ws::Message message
impl StreamHandler<Result<ws::Message, ws::ProtocolError>> for CameraWs {
    fn handle(&mut self, msg: Result<ws::Message, ws::ProtocolError>, ctx: &mut Self::Context) {
        //debug!("got ws message: {:?}", msg);
        match msg {
            Ok(ws::Message::Ping(msg)) => ctx.pong(&msg),
            Ok(ws::Message::Text(text)) => {}
            Ok(ws::Message::Binary(bin)) => {}
            Ok(ws::Message::Close(_)) => ctx.stop(),
            _ => (),
        }
    }
}

#[derive(Message, Clone)]
#[rtype(result = "()")]
pub struct CameraFrame(pub Vec<u8>);

impl Handler<CameraFrame> for CameraWs {
    type Result = ();

    fn handle(&mut self, msg: CameraFrame, ctx: &mut Self::Context) {
        ctx.binary(msg.0);
    }
}
