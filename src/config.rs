use serde::Deserialize;
use std::fs;

#[derive(Debug, Deserialize, Clone)]
pub struct Config {
    pub width: u16,
    pub height: u16,
    pub fps: u8,
    pub buffer: u8,
}

impl Config {
    pub fn new() -> Self {
        match Self::read() {
            Ok(config) => config,
            Err(_) => Self {
                width: 1024,
                height: 576,
                fps: 20,
                buffer: 4,
            },
        }
    }

    fn read() -> Result<Self, Box<dyn std::error::Error>> {
        let contents = fs::read_to_string("h264rcam.toml")?;
        Ok(toml::from_str(&contents)?)
    }
}
