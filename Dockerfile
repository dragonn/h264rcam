FROM rustembedded/cross:arm-unknown-linux-gnueabihf

RUN dpkg --add-architecture armhf && \
    apt-get update && \
    apt-get install --assume-yes wget gnupg clang

RUN echo  "deb http://archive.raspberrypi.org/debian/ buster main" >> /etc/apt/sources.list && \
    wget -qO - https://archive.raspberrypi.org/debian/raspberrypi.gpg.key | apt-key add - && \
    apt-get update

RUN cat /etc/os-release && \
    apt-cache search raspberrypi && \
    apt-get install --assume-yes libraspberrypi-bin libraspberrypi0